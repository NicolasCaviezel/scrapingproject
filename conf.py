import os


URL = "https://www.avendrealouer.fr/recherche.html?pageIndex=1&sortPropertyName=ReleaseDate&sortDirection=Descending&" \
      "searchTypeID=2&typeGroupCategoryID=6&transactionId=2&localityIds=3-75&typeGroupIds=47,48,56"
URL_PREFIX = "https://www.avendrealouer.fr"

ARRONDISSEMENT_PATTERN = 'paris [0-9\s]*'
SURFACE_PATTERN = 'surface: [0-9.,\s]*'
ELEVATOR_PATTERN = 'ascenseur'
ROOMS_PATTERN = 'pi[eè]ce[s()]*: [\s0-9]*'
FLOOR_PATTERN = 'etage du bien: [\s0-9,.]*'
PARKING_PATTERN = 'garage|parking'
RENT_PATTERN = 'loyer mensuel: [\s0-9,.]*'
UTILITIES_INCL_PATTERN = 'charges comprises'
UTILITIES_PATTERN = 'charges mensuelles: [\s0-9,.]*'
GUARANTEE_PATTERN = 'garantie: [0-9\s,.]*|garanties: [0-9\s,.]*'

PAGE_INDEX_PREFIX = "https://www.avendrealouer.fr/recherche.html?pageIndex="
PAGE_INDEX_SUFFIX = "&sortPropertyName=ReleaseDate&sortDirection=Descending&searchTypeID=2&typeGroupCategoryID=" \
                    "6&transactionId=2&localityIds=3-75&typeGroupIds=47,48,56"

FEATURES = [
    'title',
    'arrondissement',
    'surface',
    'elevator',
    'rooms',
    'floor',
    'parking',
    'rent',
    'utilities_incl',
    'utilities',
    'guarantee',
    'agency_name',
    'agency_address',
    'agency_phone',
    'ref'
]

SAVE_FILE = '/data/'

DRIVER_PATH_LOCAL = os.path.abspath('./chromedriver')
DRIVER_PATH_INSTANCE = os.path.abspath('./driver/chromedriver')

CORRECT_ARRONDISSEMENT_PATTERN = '75[0-9]{3}'
