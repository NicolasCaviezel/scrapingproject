from scraper import Scraper
from tools import pickleize
from dfcleaner import DfCleaner
import argparse
import os


def argument_parsing():
    parser = argparse.ArgumentParser(description='Scrape specific pages from PAP.fr, enter either:'
                                                 '\n- The maximum number of pages to scrape or'
                                                 '\n- The beginning and ending pages from which to scrape')
    parser.add_argument('--pages', help='Page number or beginning page + ending page', type=int, nargs='+')
    parser.add_argument('--instance',
                        help='Enable using Ubuntu ChromeDriver (instance usage)',
                        default=False,
                        action='store_true')
    args = parser.parse_args()
    return args


def scraping(args):
    print('-' * 50)
    print('Scraping the data...')
    my_scraper = Scraper(args.pages, args.instance)
    my_scraper._save_dataset('data_final.pkl')
    print('Scraping done.')
    print('-' * 50)
    return my_scraper


def cleaning(df):
    print('-' * 50)
    print('Cleaning the data...')
    my_cleaner = DfCleaner(path_to_df=os.path.abspath('./data/data_df.pkl'), df=df)
    my_cleaner()
    my_cleaner.save_to_csv()
    print('Done.')
    print('-' * 50)


def main():
    # Defining a parser
    args = argument_parsing()

    # Defining a Scraper and scraping the data
    my_scraper = scraping(args)

    # Saving both the data and the scraper
    print('-' * 50)
    print('Saving the data...')
    df = my_scraper.get_df()
    pickleize(file_to_save=df, file_name='data_df.pkl')
    pickleize(file_to_save=my_scraper, file_name='scrapper.pkl')
    print('Done.')
    print('-' * 50)

    # Cleaning the DataFrame to make it ready to analyse
    cleaning(df)


if __name__ == '__main__':
    main()
