from bs4 import BeautifulSoup
import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from functools import reduce
import unidecode
from conf import URL_PREFIX
import pickle


def get_soup_from_url(url, driver_path):
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--incognito")

    driver = webdriver.Chrome(executable_path=os.path.join(os.path.expanduser('~'), driver_path),
                              options=chrome_options)
    driver.implicitly_wait(15)
    driver.get(url)
    soup = BeautifulSoup(driver.page_source, 'lxml')
    return soup


def get_general_infos(announce_soup):
    infos_list = announce_soup.select('td span')
    infos_list[0] = infos_list[0].get_text()
    return reduce(lambda x, y: str(x) + y.get_text(), infos_list)


def get_pricing_infos(announce_soup):
    return standardize_string(announce_soup.select('.pricing-data')[0].get_text())


def standardize_string(string: str):
    return unidecode.unidecode(string)


def string_to_float(string: str):
    try:
        return float(unidecode.unidecode(string.replace(',', '').replace(' ', '')))
    except ValueError:
        return None


def make_announce_links(soup):
    return [URL_PREFIX + link['href'] for link in soup.find_all('a', class_='linkCtnr linkToFd')]


def true_list_len(l: list):
    i = 0
    while l[i]:
        i += 1
    return i


def pickleize(file_to_save, file_name: str):
    with open(os.path.abspath(os.path.join('./data', file_name)), 'wb') as f:
        pickle.dump(file_to_save, f)


def clean_list(l: list):
    counter = 0
    for elt in l:
        if not elt:
            counter += 1
    if counter > 0:
        cleaned_list = [None] * (len(l) - counter)
        counter = 0
        for i in range(len(l)):
            if l[i]:
                cleaned_list[counter] = l[i]
                counter += 1
        return cleaned_list
    else:
        return l


def load_from_pickle(path):
    with open(os.path.abspath(path), 'rb') as f:
        data = pickle.load(f)
    return data