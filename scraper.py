from conf import *
from tools import *
import re
import pickle
import os
from tqdm import tqdm
import pandas as pd
import time


class Scraper:
    def __init__(self, args: list, on_instance=False):
        """
        Scraper constructor
        :param args: list of pages either of length 1 : max page or 2: min page/ max page to scrape
        :param on_instance: boolean spcifying if the code is running on a remote instance hence locate webdriver
        """
        self.driver_path = DRIVER_PATH_INSTANCE if on_instance else DRIVER_PATH_LOCAL

        if not args or len(args) == 0:
            self.start_page = 1
            self.end_page = 1e3
        elif len(args) == 1:
            self.start_page = 1
            self.end_page = args[0]
        elif len(args) == 2:
            self.start_page = min(args)
            self.end_page = max(args)
        else:
            raise ValueError('You must pass either a page number or two pages numbers (start/end)')

        self._set_len()
        self._dataset = [None] * self._len
        self._make_dataset()

    def _set_len(self):
        """
        Set the length of the dataset according to the number of announces to scrape
        """
        try:
            soup = get_soup_from_url(URL, driver_path=self.driver_path)
            max_page = int(soup.find_all('a', class_='pageNumber')[-1]['data-page-number'])

            if self.end_page > max_page:
                print('The highest number of found pages is {}... Setting the maximum page to scrape to it'
                      .format(max_page))
                self.end_page = max_page

            self._len = \
                (self.end_page - self.start_page) * 25 + \
                len(make_announce_links(get_soup_from_url(PAGE_INDEX_PREFIX + str(self.end_page) + PAGE_INDEX_SUFFIX,
                                                          driver_path=self.driver_path)))

        except AttributeError as e:
            print(e.args[0])

    def _make_dataset(self):
        """
        Scraping all the pages in order to create the dataset
        """
        start = time.time()

        try:
            for page_number in tqdm(range(self.start_page, self.end_page + 1)):
                announces_soup = get_soup_from_url(PAGE_INDEX_PREFIX + str(page_number) + PAGE_INDEX_SUFFIX,
                                                   driver_path=self.driver_path)
                self._scrape_page(make_announce_links(announces_soup), page_number)

        except AttributeError as e:
            print(e.args[0])
            print('Problem while extracting the soup')
            self._actual_len = true_list_len(self._dataset)
            self._save_dataset('data_tmp_{}.pkl'.format(len(self._dataset)))

        finally:
            print('*' * 50)
            print('Elapsed time creating dataset : {}sec'.format(time.time()-start))

    def _scrape_page(self, announces_link: list, page_number):
        """
        Scrape all the announces of a page (up to 25)
        :param announces_link: the list of URLS
        :param page_number: the number of the page to scrape
        """
        for i, announce_link in enumerate(announces_link):
            self._dataset[int((page_number-1) * 25 + i)] = self._extract_features(announce_link)

    def _extract_features(self, announce_url):
        """
        Extract all features from a given announce
        :param announce_url: the URL of the announce to scrape
        :return: dict of features
        """

        try:
            soup = get_soup_from_url(announce_url, driver_path=self.driver_path)
            features = {feature_name: self._get_feature(feature_name=feature_name, announce_soup=soup)
                        for feature_name in FEATURES
                        if feature_name != 'link'}
            features['link'] = announce_url
            return features

        except ValueError as e:
            print(e.args)
            print('On ' + announce_url)
            return None

        except IndexError as e:
            print(e.args)
            print('On' + announce_url)

        except AttributeError as e:
            print(e.args)
            print('On' + announce_url)

    def _get_feature(self, feature_name: str, announce_soup):
        """
        Extract a specific feature from an announce soup
        :param feature_name: the name of the feature to extract (of type str)
        :param announce_soup: the bs4.BeautifulSoup of the page
        :return: the feature value
        """
        try:
            if feature_name == 'title':
                return announce_soup.find('span', class_='mainh1').get_text()

            if feature_name == 'arrondissement':
                match = re.search(ARRONDISSEMENT_PATTERN,
                                  self._get_feature(announce_soup=announce_soup, feature_name='title'), re.IGNORECASE)
                if not match:
                    raise ValueError('Arrondissement not found')
                return match.group().split()[-1]

            if feature_name == 'surface':
                match = re.search(SURFACE_PATTERN, get_general_infos(announce_soup), re.IGNORECASE)
                if not match:
                    raise ValueError('Surface not found')
                return string_to_float(match.group().split()[-1])

            if feature_name == 'elevator':
                return True if re.search(ELEVATOR_PATTERN, get_general_infos(announce_soup), re.IGNORECASE) else False

            if feature_name == 'rooms':
                match = re.search(ROOMS_PATTERN, get_general_infos(announce_soup), re.IGNORECASE)
                if not match:
                    raise ValueError('Rooms not found')
                return string_to_float(match.group().split()[-1])

            if feature_name == 'floor':
                match = re.search(FLOOR_PATTERN, get_general_infos(announce_soup), re.IGNORECASE)
                if not match:
                    return None
                return string_to_float(match.group().split()[-1])

            if feature_name == 'parking':
                return True if re.search(PARKING_PATTERN, get_general_infos(announce_soup), re.IGNORECASE) else False

            if feature_name == 'rent':
                match = re.search(RENT_PATTERN, get_pricing_infos(announce_soup), re.IGNORECASE)
                if not match:
                    raise ValueError('Rent not found')
                return string_to_float(match.group().split(':')[-1])

            if feature_name == 'utilities_incl':
                return True if re.search(UTILITIES_INCL_PATTERN, get_pricing_infos(announce_soup),
                                         re.IGNORECASE) else False

            if feature_name == 'utilities':
                match = re.search(UTILITIES_PATTERN, get_pricing_infos(announce_soup), re.IGNORECASE)
                return string_to_float(match.group().split(':')[-1]) if match else None

            if feature_name == 'guarantee':
                match = re.search(GUARANTEE_PATTERN, get_pricing_infos(announce_soup), re.IGNORECASE)
                return string_to_float(match.group().split(':')[-1]) if match else None

            if feature_name == 'agency_name':
                return announce_soup.find('div', class_='agency-title').get_text().strip(' \n')

            if feature_name == 'agency_address':
                return announce_soup.find('div', class_='agency-address').get_text().strip('\n ')

            if feature_name == 'agency_phone':
                return announce_soup.find('div', class_='btnDefault')['data-phone-number'].replace(' ', '')

            if feature_name == 'ref':
                return announce_soup.find('p', class_='property-reference').get_text().split(':')[-1].strip(' \n')

        except TypeError:
            return None

    def _save_dataset(self, file_name):
        """
        Pickelize the dataset
        :param file_name: the name of the file where to save the dataset
        """
        with open(os.path.abspath(os.path.join('./data/', file_name)), 'wb') as f:
            pickle.dump(self._dataset, f)

    def get_df(self):
        """
        Create a pandas.DataFrame from the dataset
        :return: a pandas.DataFrame dataset
        """
        return pd.DataFrame(columns=FEATURES, data=clean_list(self._dataset))
