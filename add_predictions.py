from price_predictor import BestModelPrediction
from tools import pickleize
import os
import pickle
import pandas as pd
from copy import deepcopy


PATH_TO_FINAL_DATA = os.path.abspath('./data/final_data.pkl')


def read_from_pickle(path):
    with open(path, 'rb') as f:
        return pickle.load(f)


def main():
    # Finding best prediction model
    print('-' * 50)
    print('Defining the best prediction model...')
    predictor = BestModelPrediction()
    predictor._find_best_models()
    print('Done.')
    print('-' * 50)

    # Augmenting dataset with predictions
    print('-' * 50)
    print('Augmenting the data')
    augmented_data = predictor.append_to_data(predictor.preprocessor.df)
    augmented_data['arrondissement'] = augmented_data['arrondissement'].astype(int)
    print('Done')
    print('-' * 50)

    # Saving final data
    print('-' * 50)
    print('Saving final the data')
    pickleize(file_name=PATH_TO_FINAL_DATA,
              file_to_save=pd.merge(
                  augmented_data,
                  pd.read_csv(predictor.preprocessor.path_to_api_data),
                  left_on='arrondissement',
                  right_on='Unnamed: 0'
                  ).drop(columns='Unnamed: 0').drop_duplicates(subset='ref')
              )
    print('Done.')
    print('-' * 50)


if __name__ == '__main__':
    main()
