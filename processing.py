import pandas as pd
from tools import *
from copy import deepcopy


ARRONDISSEMENT_PREFIX = 'arrondissement '
PATH_TO_DATA = './data/data_df_cleaned.pkl'
PATH_TO_API = './data/api_data.csv'


class Preprocessing:
    def __init__(self, path_to_api_data=PATH_TO_API, path_to_data=PATH_TO_DATA):
        self.path_to_api_data = path_to_api_data
        self.path_to_data = path_to_data
        self.df = None
        self.data = None

    def _enrich_data(self):
        self.df = load_from_pickle(self.path_to_data)
        df_api = pd.read_csv(self.path_to_api_data)
        df_api = df_api.rename(
            columns={col: col if col != 'Unnamed: 0' else 'arrondissement' for col in df_api.columns})
        df_api['arrondissement'] = df_api['arrondissement'].apply(str)
        self.data = pd.merge(self.df, df_api, left_on='arrondissement', right_on='arrondissement')

    def _to_dummies(self):
        self.data = self.arrondissement_to_dummies(self.data)
        self.data = self.floor_to_dummies(self.data)

    def _make_data(self):
        tmp = deepcopy(self.data).drop(
            columns=['title',
                     'agency_name',
                     'agency_address',
                     'agency_phone',
                     'ref',
                     'agency_id'
                     ] +
                    ['arrondissement {}'.format(i) for i in range(1, 21)]

        )
        return tmp.drop(columns='rent').astype(float), self.data['rent']

    def _make_data_no_api(self):
        tmp = self.data.drop(
            columns=['title',
                     'agency_name',
                     'agency_address',
                     'agency_phone',
                     'ref',
                     'agency_id',
                     'dynamic',
                     'facilities',
                     'luxury',
                     'shop'] +
                    ['arrondissement {}'.format(i) for i in range(1, 21)]
        )
        return tmp.drop(columns='rent').astype(float), self.data['rent']

    def __call__(self, enriched=True):
        if enriched:
            self._enrich_data()
        self._to_dummies()
        if enriched:
            return self._make_data()
        return self._make_data_no_api()

    @staticmethod
    def arrondissement_to_dummies(df):
        dums = pd.get_dummies(df['arrondissement'])
        dums = dums.rename(columns={col: ARRONDISSEMENT_PREFIX + str(col) for col in dums.columns})
        tmp = pd.concat([df, dums], axis=1)
        tmp = tmp.drop(columns='arrondissement')
        return tmp

    @staticmethod
    def floor_to_dummies(df):
        df[df['floor'] > 5] = -1.0
        dums = pd.get_dummies(df['floor'])
        dums = dums.rename(
            columns={col: 'floor ' + str(int(col)) if col > 0 else 'floor > 5' for col in dums.columns})
        tmp = pd.concat([df, dums], axis=1)
        tmp = tmp.drop(columns='floor')
        return tmp
