import pandas as pd
import pickle
import re
import os
from conf import CORRECT_ARRONDISSEMENT_PATTERN


class DfCleaner:
    def __init__(self, path_to_df: str, df=None):
        """
        DfCleaner constructor
        :param path_to_df: absolute path to the pd.DataFrame
        """
        if not isinstance(df, pd.core.frame.DataFrame):
            with open(path_to_df, 'rb') as f:
                self.df = pickle.load(f)
        else:
            self.df = df
        self.path_to_df = path_to_df

    def __call__(self):
        self.clean()
        self._save_clean_df()

    def clean(self):
        feature_to_correct = self._get_features_to_clean()

        if 'arrondissement' in feature_to_correct:
            self._correct_arrondissement()

        if 'guarantee' in feature_to_correct:
            self._correct_guarantee()

        if 'utilities' in feature_to_correct:
            self._correct_utilities()

        if 'floor' in feature_to_correct:
            self._correct_floor()

        if 'agency_id' not in self.df.columns:
            self._add_agency_id()

    def _get_features_to_clean(self):
        features_to_correct = self.df.columns[self.df.isnull().any()].tolist()
        if len(self.df[self.df['arrondissement'] == 'Paris']) > 0:
            features_to_correct.append('arrondissement')
        return features_to_correct

    def _correct_arrondissement(self):
        for idx in self.df[self.df['arrondissement'] == 'Paris'].index:
            self.df.loc[idx, 'arrondissement'] = \
                re.search(CORRECT_ARRONDISSEMENT_PATTERN, self.df.loc[idx, 'title']).group()[-2:]

    def _correct_guarantee(self):
        correction_factor = self.get_mean_proportion(feature_to_correct='guarantee')
        for idx in self.df[self.df.guarantee.isnull()].index:
            self.df.loc[idx, 'guarantee'] = correction_factor * self.df.loc[idx, 'rent']

    def _correct_utilities(self):
        correction_factor = self.get_mean_proportion(feature_to_correct='utilities')
        for idx in self.df[self.df.utilities.isnull()].index:
            self.df.loc[idx, 'utilities'] = correction_factor * self.df.loc[idx, 'rent']

    def _correct_floor(self):
        self.df.loc[self.df.floor.isnull(), 'floor'] = 0.0

    def get_mean_proportion(self, feature_to_correct: str):
        prop = 0

        for idx in self.df[feature_to_correct].index[self.df[feature_to_correct].apply(pd.notnull)]:
            prop += self.df.loc[idx, feature_to_correct] / self.df.loc[idx, 'rent']

        prop /= len(self.df[feature_to_correct].index[self.df[feature_to_correct].apply(pd.notnull)])
        return prop

    def _add_agency_id(self):
        self.df['agency_name'].unique()
        dictionary = dict()
        for i, x in enumerate(self.df['agency_name'].unique().tolist()):
            dictionary[x] = i
        self.df['agency_id'] = pd.Series()
        for i in range(len(self.df)):
            self.df.loc[i, 'agency_id'] = dictionary[self.df['agency_name'][i]]

    def _save_clean_df(self):
        with open(os.path.abspath(self.path_to_df)[:-4]+'_cleaned.pkl', 'wb') as f:
            pickle.dump(self.df, f)

    def save_to_csv(self):
        self.df.to_csv(os.path.abspath(self.path_to_df).split('df')[0]+'cleaned.csv')
