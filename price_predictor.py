from processing import Preprocessing
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import os
import xgboost as xgb
import numpy as np
import pickle
import pandas as pd
from tools import clean_list
# from sklearn.metrics import mean_squared_error


FEATURES = [
    'title',
    'arrondissement',
    'surface',
    'elevator',
    'rooms',
    'floor',
    'parking',
    'rent',
    'utilities_incl',
    'utilities',
    'guarantee',
    'agency_name',
    'agency_address',
    'agency_phone',
    'ref',
    'link'
]
XGB_PARAMS = {
    'eta': .5,
    'seed': 0,
    'subsample': 1,
    'colsample_bytree': .5,
    'objective': 'reg:linear',
    'max_depth': 20,
    'min_child_weight': 1
}
MODELS = ['Linear Model', 'XGBoost']
PATH_TO_MODEL = os.path.abspath('./model/model.pkl')


class BestModelPrediction:
    def __init__(self):
        self.preprocessor = Preprocessing()
        self.model = None
        self.model_name = None

    def _find_best_models(self):
        x, y = self.preprocessor()
        for model_name in MODELS:
            x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=.1)

            best_score = 1e6
            # best_MSE = 1e6

            if model_name == 'Linear Model':
                model = LinearRegression(normalize=True)
                model.fit(x_train, y_train)
                y_pred = model.predict(x_test)

            # elif to let the possibility to add futures models
            # elif model_name == 'XGBoost':
            else:
                xgdmat = xgb.DMatrix(x_train.values, y_train.values)
                model = xgb.train(XGB_PARAMS, xgdmat)
                tesdmat = xgb.DMatrix(x_test.values)
                y_pred = model.predict(tesdmat)

            score = np.abs(y_pred - y_test).mean()
            # MSE = mean_squared_error(y_test, y_pred)

            if score < best_score:
                best_score = score
                self.model_name = model_name
                self.model = model
                self._save_model(PATH_TO_MODEL)

            print('The best found model is: {} with L1 dist of {}'.format(self.model_name, best_score))

    def _save_model(self, path_to_file):
        with open(path_to_file, 'wb') as f:
            pickle.dump(self.model, f)

    def _load_model(self, path_to_model):
        with open(path_to_model, 'rb') as f:
            self.model = pickle.load(f)

    def predict_price(self, x):
        if self.model_name == 'XGBoost':
            x = xgb.DMatrix(x.values)
        return self.model.predict(x)

    def append_to_data(self, data):
        x, y = self.preprocessor()
        if not self.model:
            self._find_best_models()
        with open(os.path.abspath(r'/Users/nico/Documents/Cours/ITC/ScrapingProject/data/data_final.pkl'),
                  'rb') as f:
            additional_df = self.make_dataframe(pickle.load(f))
        big_df = pd.merge(data, additional_df.loc[:, ['link', 'ref']], on='ref')
        big_df['Interesting'] = self.predict_price(x) < y
        self.add_description(big_df)
        big_df = big_df.iloc[:615, :]
        return big_df

    def add_description(self, data):
        data['description'] = None
        for i in range(len(data)):
            data.loc[i, 'description'] = self.describe(data.iloc[i, :])

    @staticmethod
    def make_dataframe(l: list):
        return pd.DataFrame(columns=FEATURES, data=clean_list(l))

    @staticmethod
    def describe(line):
        desc = 'It is a lovely appartment of {} m² with {} rooms, located in the {}th arrondissement. '.format(
            line['surface'], int(line['rooms']), line['arrondissement'])
        if line['Interesting']:
            desc = 'I have found an interesting appartment, the price is great!' + desc
        desc += ' It costs only {}€ per month with {}€ of utilities'.format(int(line['rent']), int(line['utilities']))
        if line['utilities_incl']:
            desc += ' included'
        desc += '. More infos at : {}'.format(line['link'])
        return desc
